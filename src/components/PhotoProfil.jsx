import styles from './PhotoProfil.module.css';
import profil from '../ressources/profil.webp'

export default function PhotoProfil (){
    return <div className={styles.photoprofil}>
                <img src={profil} alt="Robert_image" />
            <div className={styles.skills}>
                <p> Développeur web fullstack </p>
                <p> Développeur d'application mobile </p>
                <p> Développeur d'application de bureau </p>
            </div>
                
           </div>
}