import { useState } from "react";
import { IntlProvider } from "react-intl";
import EnglishData from "../Translation/en.json";
import FrenchData from "../Translation/fr.json";


const translation = {
    en: EnglishData,
    fr: FrenchData
}

export function LocaleProvider(props) {
    const [locale] = useState('en');
    return <IntlProvider locale={locale} messages={translation[locale]} >
        {props.children}
    </IntlProvider>
}