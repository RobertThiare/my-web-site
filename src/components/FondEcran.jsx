import fondEcran from '../ressources/fondEcran.jpg'
export default function FondEcran(){
    return <div>
        <img src={fondEcran} alt="Fond d'écran" />
    </div>
}