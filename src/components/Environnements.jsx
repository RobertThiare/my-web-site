import visual from '../ressources/vscode.png';
import docker from '../ressources/docker.png';
import sublime from '../ressources/sublime-text.png';
import android from '../ressources/android-studio.png';
import git from '../ressources/git.png';
import nodejs from '../ressources/nodejs.png';
import sqlite from '../ressources/sqlite.png';
import sqlserver from '../ressources/sql.png';
import visualcode from '../ressources/visualstudio.png';
import xcode from '../ressources/xcode.png';

import styles from './Envirronements.module.css';
export default function Environnements() {
    return <div className={styles.envirronement}>
                <div className={styles.total}>
                    <a href="https://docs.microsoft.com/en-us/visualstudio/windows/?view=vs-2022">
                        <img src={visual} alt="Visual studio" />
                    </a>
                    <label>Visual Studio</label>
                </div>
                <div className={styles.total}>
                    <a href="https://docs.docker.com/">
                        <img src={docker} alt="Docker" />
                    </a>
                    <label>Docker</label>
                </div>
                <div className={styles.total}>
                    <a href="https://www.sublimetext.com/docs/">
                        <img src={sublime} alt="Sublime Texte" />
                    </a>
                    <label>Sublime texte</label>
                </div>
                <div className={styles.total}>
                    <a href="https://developer.android.com/docs">
                        <img src={android} alt="Android studio" />
                    </a>
                    <label>Android Studio</label>
                </div>
                <div className={styles.total}>
                    <a href="https://git-scm.com/doc">
                        <img src={git} alt="Git" />
                    </a>
                    <label>Git</label>
                </div>
                <div className={styles.total}>
                    <a href="https://www.sqlite.org/docs.html">
                        <img src={sqlite} alt="Sqlite" />
                    </a>
                    <label>Sqlite</label>
                </div>
                <div className={styles.total}>
                    <a href="https://docs.microsoft.com/en-us/sql/?view=sql-server-ver16">
                        <img src={sqlserver} alt="Microsoft SQL Server" />
                    </a>
                    <label>SQL Server</label>
                </div>
                <div className={styles.total}>
                    <a href="https://code.visualstudio.com/Docs">
                        <img src={visualcode} alt="Visual studio" />
                    </a>
                    <label>Visual studio code</label>
                </div>
                <div className={styles.total}>
                    <a href="https://developer.apple.com/documentation/xcode">
                        <img src={xcode} alt="Xcode" />
                    </a>
                    <label>Xcode</label>
                </div>
                <div className={styles.total}>
                    <a href="https://nodejs.org/en/docs/">
                        <img src={nodejs} alt="node js" />
                    </a>
                    <label>Node JS</label>
                </div>
            </div>
}