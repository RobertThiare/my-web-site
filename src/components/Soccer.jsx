import p1acc from '../ressources/P1Acc.PNG';
import p1q1 from '../ressources/P1Q1.PNG';
import p1q2 from '../ressources/P1Q2.PNG';
import p1q3 from '../ressources/P1Q3.PNG';
import p1q4 from '../ressources/P1Q4.PNG';
import soccer from '../ressources/Soccer-Quizz.zip';

import ContentToggler from './ContentToggler';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import styles from './Soccer.module.css';

export default function Soccer() {
    const datas = [
        {
            id: 1,
            image: p1acc,
            title: "Saisi du nom d'utilisateur"
        },
        {
            id: 2,
            image: p1q1,
            title: "Question 1"
        },
        {
            id: 3,
            image: p1q2,
            title: "Question 2"
        },
        {
            id: 4,
            image: p1q3,
            title: "Question 3"
        },
        {
            id: 5,
            image: p1q4,
            title: "Question 4"
        }
    ]

        return <div>
            <div className={styles.texte}>
               <h1>Quizz de football</h1>
               <p>
                    Le projet Soccer Quizz a été développé dans le cadre du cours
                    de programmation web client. C'est un jeu qui consiste à
                    deviner la réponse des différentes questions posées.
               </p>
               <p>
                    Mode de fonctionnement:
               </p>
               <p>
                    L'utilisateur saisi son nom et clique sur le bouton commencer.
                    Il va ensuite regarder l'image et la question qui lui a été posée
                    pour deviner la bonne réponse parmi les quatre qui lui sont 
                    proposées. 
               </p>
               <p>
                    Après avoir répondu à toutes les questions, un score et
                    un niveau de satisfaction sont attribués au joeur.
               </p>
               <p>Technologies</p>
               <p>
                HTML5,
                CSS3,
                JavaScript
               </p>
               <p>
                    <a href={soccer} download>Code source</a>
               </p>
            </div>
                <ContentToggler title="Cliquer pour voir les images">
                    <Carousel>
                                {
                                    datas.map(slide => (
                                        <div key={slide.id}>
                                            <img src={slide.image} alt={slide.title} />
                                        </div>
                                    ))
                                }

                    </Carousel>
                </ContentToggler>
                </div>
}