import compte from '../ressources/Compte.PNG';
import Soccer from './Soccer';
import ContentToggler from './ContentToggler';
import crationCompte from '../ressources/creationCompte.png';
import poste from '../ressources/poste.PNG';
import recherche from '../ressources/Recherche.PNG';
import user from '../ressources/user.PNG';
import instatChart from '../ressources/projetWebServeur.zip';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import styles from './ProjetsWebs.module.css';

export default function ProjetsWebs() {
    const datas = [
        {
            id: 1,
            image: compte,
            title: "Page de création de compte"
        },
        {
            id: 2,
            image: crationCompte,
            title: "Page de création de compte"
        },
        {
            id: 3,
            image: poste,
            title: "Page de publication"
        },
        {
            id: 4,
            image: recherche,
            title: "Page de recherche"
        },
        {
            id: 5,
            image: user,
            title: "Page d'utilisateur"
        }
       
    ];
    return <div className={styles.carousel}>
                <div className={styles.texte}>
                    <h1>Insta-Chart</h1>
                    <p>
                    le projet Insta-Chart a été développé dans le cadre
                    du cours de programmation de serveur web.
                    </p>
                    <p><strong>Fonctionnement</strong></p>
                    <p>
                    Il consiste à créer une application qui permet aux 
                    utilisateurs de faire des publications instantanées dans 
                    le site, de rechercher d'autres utilisateurs, de les suivre 
                    et d'ajouter des "J'aime" à une ou plusieurs publications.
                    Tous les utilisateurs devront créer un compte et s'authentifier
                    pour communiquer avec le serveur.
                    </p>
                    <p>
                    Il existe deux catégories de compte. Tous les utilisateurs
                    qui se crée un compte standard. Cependant, l'administrateur
                    peut changer leur niveau d'accès afin qu'ils puissent avoir 
                    d'autres privilèges comme supprimer des publications.
                    </p>
                    <p>
                    <strong>Technologies utilisées</strong>
                    </p>
                    Node js,
                    JavaScript,
                    Express js,
                    Handlebars,
                    HTML5,
                    CSS3,
                    SQLite
                    <p>
                        <a href={instatChart} download>Code source</a>
                    </p>
                </div>
                <div className={styles.carousel}>
                <ContentToggler title="Cliquer pour voir les images">
                    <Carousel>
                                {
                                    datas.map(slide => (
                                        <div key={slide.id}>
                                            <img src={slide.image} alt={slide.title} />
                                        </div>
                                    ))
                                }

                    </Carousel>
                </ContentToggler>
                </div>
                <Soccer />
    </div>
}