import cv from '../ressources/CV-officiel.pdf';

import styles from './Cv.module.css';

export default function Cv() {
    return <div className={styles.cv}>
        <a href={cv} download>
            Télécharger mon CV ici
        </a>
    </div>
}