import ContentToggler from './ContentToggler';
import ListeNation from './ListeNation';
import accueil from '../ressources/Accueil.PNG';
import page2 from '../ressources/page2.PNG';
import page3 from '../ressources/page3.PNG';
import page4 from '../ressources/page4.PNG';
import page5 from '../ressources/page5.PNG';
import patate from '../ressources/Robby.zip'; 

import styles from './ProjetsMobiles.module.css';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

export default function ProjetsMobiles() {
    
    const datas = [
        {
            id: 1,
            image: accueil,
            title: "Page d'accueil"
        },
        {
            id:2,
            image: page2,
            title: "Page d'items"
        },
        {
            id:3,
            image: page3,
            title: "Page du map"
        },
        {
            id:4,
            image: page4,
            title: "Page commander"
        },
        {
            id:5,
            image: page5,
            title: "Page facture"
        }
    ]
    return <div className={styles.projetsmobiles}>
        <div className={styles.texte}>
            <p>
                <h1>À la bonne patâte</h1>
            </p>
            <p>
                C'est un projet réalisé dans le cadre du cours 
                d'applications mobiles IOS. Il consiste à créer 
                une application de vente en ligne.  
            </p>
            <p><strong>Fonctionnement</strong></p>
            <p>
                L'utilisateur doit, après avoir ouvert l'application,
               déplacer le bouton de la gauche vers la droite pour 
               accéder à la page de commande.
               Dans cette page, il peut choisir les items qu'il veut commander.
               Il peut vérifier le prix total de sa commande dans Facture,
               ajouter une adresse de livraison dans Livraison ou appuyer sur
               commander dans Paiement.
               L'utilisateur peut aussi voir l'historique de ses commandes.
            </p>
            <p><strong>Technologies utilisées</strong></p>
            <p>
                Switf
            </p>
            <p>
                <a href={patate} download>Code source</a>
            </p>
        </div>
            <ContentToggler title="Cliquer pour voir les images">
                <Carousel>
                            {
                                datas.map(slide => (
                                    <div key={slide.id}>
                                        <img src={slide.image} alt={slide.title} />
                                    </div>
                                ))
                            }

                </Carousel>
            </ContentToggler>
            <ListeNation />

    </div>
}