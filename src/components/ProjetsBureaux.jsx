import ContentToggler from './ContentToggler';
import inter1 from '../ressources/Inter1.PNG';
import inter2 from '../ressources/Inter2.PNG';
import inter3 from '../ressources/Inter3.PNG';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import styles from './ProjetsBureaux.module.css';

export default function ProjetsBureaux() {
    
    const datas = [
        {
            id: 1,
            image: inter1,
            title: "Page de gestion des programme"
        },   
        {
            id: 2,
            image: inter2,
            title: "Page de gestion des stagiaires"
        },
        {
            id: 1,
            image: inter3,
            title: "Page de la liste des stagiaires par programme"
        }
    ]
    return <div>
        <div className={styles.texte}>
                <p>
                Ce logiciel a été créé dans le cadre d'un
                examen de fin de session au collège
                La Cité à Ottawa.
                Il consistait à créer une application de bureau
                qui va gérer les stagiaires inscrites dans un 
                programme.
                </p>

                <p>
                   <strong> Fonctionnement </strong>
                </p>
                <p>
                    Dans la section programme, l'utilisateur peut 
                    ajouter ou supprimer un programme. Les
                    programmes seront ajoutés à temps réel dans la
                    section liste des programmes.
                    Dans la section "Stagiaire", l'utilisateur peut 
                    ajouter un stagiaire ou en supprimer un via 
                    le numéro de stagiaire.
                    Dans la section "Consulter", l'utilisateur peut
                    rechercher tous les stagiaire inscrits dans un
                    programme.
                </p>
                <p>
                    <strong>Technologies</strong> 
                </p>
                <p>
                    Langage C#, Windows Presentation Foundation (WPF) ET XAML, Microsoft SQL Server  
                </p>
            </div>
            <ContentToggler title="Cliquer pour voir les images">
                <Carousel>
                            {
                                datas.map(slide => (
                                    <div key={slide.id}>
                                        <img src={slide.image} alt={slide.title}  />
                                    </div>
                                ))
                            }

                </Carousel>
            </ContentToggler>
    </div>
}