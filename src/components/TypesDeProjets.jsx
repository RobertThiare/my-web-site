
import styles from './TypesDeProjets.module.css';

export default function TypesDeProjets(props) {
    return <div className={styles.typeprojet}>
                <nav>
                    <ul className={styles.projets}>
                        <li>
                            <button onClick={props.changePage("projetweb")}> Projets webs</button>
                        </li>
                        <li>
                            <button onClick={props.changePage("projetmobile")}>Projets mobiles</button>
                        </li>
                        <li>
                            <button onClick={props.changePage("projetbureau")}>Projets bureaux</button>
                        </li>
                    </ul>    
                </nav>       
    </div>
}