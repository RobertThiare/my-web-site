import studio1 from '../ressources/studio1.PNG';
import studio2 from '../ressources/studio2.PNG';
import liste from '../ressources/listeNation.zip';


import ContentToggler from './ContentToggler';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import styles from './ListeNation.module.css';

export default function Soccer() {
    const datas = [
        {
            id: 1,
            image: studio1,
        },
        {
            id: 2,
            image: studio2,
        }

    ]

        return <div>
                <div className={styles.texte}>
               <h1>Liste des nations</h1>
               <p>
                    C'est un projet réalisé dans le cadre du cours d'applications
                    mobiles android. Il consiste à créer une application qui 
                    affiche tous les pays du monde et leur drapeau national.
                </p>
                <p>
                    <strong> Fonctionnement </strong>
                </p>
                <p>
                    À chaque clique d'un pays, le drapeau de ce dernier
                    affichera à l'écran et l'utilisateur pour cliquer sur 
                    le bouton Wiki pour accéder à la page Wikipédia du pays.
                </p>
                <p>
                    <strong> Technologies utilisées </strong>
                </p>
                <p>Java</p>
                <p>
                    <a href={liste} download>Code source</a>
                </p>
                </div>
                <ContentToggler title="Cliquer pour voir les images">
                    <Carousel>
                                {
                                    datas.map(slide => (
                                        <div key={slide.id}>
                                            <img src={slide.image} alt="" />
                                        </div>
                                    ))
                                }

                    </Carousel>
                </ContentToggler>
                </div>
}