import { NavLink } from 'react-router-dom';
import styles from './Pages.module.css';

export default function Pages (){
    return <nav>
              <ul className={styles.lien}>
                  <li>
                      <NavLink to="/" className={({isActive}) => isActive ? styles.actif : ''}>
                        Biographie
                      </NavLink>
                  </li>
                  <li>
                      <NavLink to="/profil" className={({isActive}) => isActive ? styles.actif : ''}>
                        Profil
                      </NavLink>
                  </li>
                  <li>
                      <NavLink to="/projets" className={({isActive}) => isActive ? styles.actif : ''}>
                        Projets
                        </NavLink>
                  </li>
              </ul>
           </nav>
}