import cassandra from '../ressources/cassandra.png';
import mongodb from '../ressources/mongodb.png';
import mysql from '../ressources/mysql.png';

import styles from './BaseDonnees.module.css';

export default function BaseDonnee() {
    return <div className={styles.basedonnees}>
                <div className={styles.element}>
                    <a href="https://cassandra.apache.org/_/index.html">
                        <img src={cassandra} alt="Cassandra" />
                    </a>
                    <label>Cassandra</label>
                </div>
                    
                <div className={styles.element}>
                    <a href="https://www.mongodb.com/docs/">
                        <img src={mongodb} alt="MongoDB" />
                    </a>
                    <label>MongoDB</label>
                </div>
                <div className={styles.element}>
                    <a href="https://dev.mysql.com/doc/">
                        <img src={mysql} alt="MySQL" />
                    </a>
                    <label>MySQL</label>
                </div>
            </div>
}