import styles from './MaBio.module.css';

export default function Biographie() {
    return <div className={styles.maBio}>
    
            <p>
                Salut, je m'appelle
            </p>
            <div className={styles.nom}>
                <p>
                    Robert Thiare
                </p>
            </div> 
            <p>
                Depuis mon enfance, j'ai toujours été une personne curieuse, qui cherchait
                toujours une explication en toutes choses. C'est la raison pour laquelle,
                après avoir réussi mon examen de fin d'études secondaires, je me suis 
                lancé dans le domaine de la technologie de l'information (IT) afin de mieux 
                comprendre l'architecture et le fonctionnement des ordinateurs.
            </p>

            <p>
                Pour atteindre mes objectifs, j'ai intégré le Centre de Formation Technique et Professionnelle 
                Sénégal-Japon. Dans cet établissement, j'ai eu la chance de recevoir une formation dans plusieurs
                domaines liés aux IT.
                Parmi ces derniers, le développement d'applications était celui qui m'intéressait le 
                plus. C'est la raison pour laquelle, j'ai décidé de poursuivre mes études dans ce 
                domaine.
            </p>

            <p>
                Ma capacité d'apprentissage et ma connaissance de l'exigence du domaine me poussent à me
                former en permanence dans le but de mettre à jour mes connaissances et à suivre l'évolution des 
                technologies utilisées. 
                Ces compétences sont de précieux atouts pour moi, car elles me permettent 
                de participer à la création d'applications performantes, facilement maintenable 
                et qui répondent aux besoins des utilisateurs.    
            </p>
            
           </div>
}