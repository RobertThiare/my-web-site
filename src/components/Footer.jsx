import Cv from "./Cv";
import Contacts from "./Contacts";

import styles from './Footer.module.css';

export default function Footer() {
    return <div className={styles.footer}>
                <Cv />
                <Contacts />
    </div>
}