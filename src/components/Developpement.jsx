import c from '../ressources/c.png';
import cplus from '../ressources/cplus.png';
import csharp from '../ressources/csharp.png';
import java from '../ressources/java.png';
import swift from '../ressources/swift.png';
import html from '../ressources/html5.png'
import css from '../ressources/css3.png'
import javaScript from '../ressources/javascript.png';
import react from '../ressources/react.png';


import styles from './Developpement.module.css';


export default function DevBureau() {
    return <div className={styles.developpement}>

                <div className={styles.total}>
                    <a href="https://docs.microsoft.com/en-us/cpp/c-language/?view=msvc-170">
                        <img src={c} alt="Langage C" />
                    </a>
                    <label>Langage C</label>
                </div>
                <div className={styles.total}>
                    <a href="https://docs.microsoft.com/en-us/cpp/cpp/cpp-language-reference?view=msvc-170">
                        <img src={cplus} alt="Langage C++" />
                    </a>
                    <label>Langage C++</label>
                </div>
                <div className={styles.total}>
                    <a href="https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/introduction">
                        <img src={csharp} alt="csharp.png" />
                    </a>
                    <label>Langage C#</label>
                </div>
                <div className={styles.total}>
                    <a href="https://docs.oracle.com/javase/7/docs/technotes/guides/language/">
                        <img src={java} alt="java.png" />
                    </a>
                    <label>Langage Java</label>
                </div>
                <div className={styles.total}>
                    <a href="https://www.w3docs.com/learn-html/html5-introduction.html">
                        <img src={html} alt="HTML5" />
                    </a>
                    <label>HTML5</label>
                </div>
                <div className={styles.total}>
                    <a href="https://www.swift.org/documentation/">
                        <img src={swift} alt="Swift" />
                    </a>
                    <label>Swift</label>
                </div>
                <div className={styles.total}>
                    <a href="https://www.w3schools.com/cssref/">
                        <img src={css} alt="Css3" />
                    </a>
                    <label>CSS3</label>
                </div>
                <div className={styles.total}>
                    <a href="https://developer.mozilla.org/fr/docs/Web/JavaScript">
                        <img src={javaScript} alt="javaScript" />
                    </a>
                    <label>JavaScript</label>
                </div>
                
                <div className={styles.total}>
                    <a href="https://reactjs.org/docs/getting-started.html">
                        <img src={react} alt="React js" />
                    </a>
                    <label>React JS</label>
                </div>
    </div>
}
