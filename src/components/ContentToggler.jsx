import { useState } from "react";
import styles from "./ContentToggler.module.css";

export default function ContentToggler(props) {
    const [visible, setVisible]= useState(false);

    const toggleContent = () => {
        setVisible(!visible);
    }
    return <div>
        <div onClick= {toggleContent} className={styles.toggler}>
        <svg className={visible ? '' : styles.inverser} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <g>
                <path d="M12,2A10,10,0,1,0,22,12,10.011,10.011,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.009,8.009,0,0,1,12,20Z"/>
                <polygon points="12 12.586 8.707 9.293 7.293 10.707 12 15.414 16.707 10.707 15.293 9.293 12 12.586"/>
            </g>
        </svg>
        { props.title }
        </div>
        {visible &&
            <div>
                {props.children}
            </div>
        }
    </div>
}