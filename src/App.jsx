import fondEcran from './ressources/fondEcran.webp';
import Projet from './pages/Projet';
import Profil from './pages/Profil';
import Biographie from './pages/Biographie';
import './App.css';
import { Routes, Route } from 'react-router';
import Layout from './pages/Layout';
import { LocaleProvider } from './components/LocalProvider';


export default function App() {

const containerStyle = fondEcran;
    
  return <div className="container" style={{backgroundImage: `url(${containerStyle})`}}>
            <LocaleProvider>
            <Routes>
                    <Route path='/' element={<Layout/>}>
                        <Route index element={<Biographie/>} />
                        <Route path='profil' element={<Profil/>} />
                        <Route path='projets' element= {<Projet />} />
                    </Route>
            </Routes>
            </LocaleProvider>
        </div>
       
  
}




