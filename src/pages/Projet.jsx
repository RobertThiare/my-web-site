import TypesDeProjets from '../components/TypesDeProjets';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import styles from './Projet.module.css';
import ProjetsWebs from '../components/ProjetsWebs';
import ProjetsBureaux from '../components/ProjetsBureaux';
import ProjetsMobiles from '../components/ProjetsMobiles';

export default function Projet() {
    const [pageParDefaut, setPageParDefaut] = useState ('projetweb');
    const changePage = (projet) => {
        return () => {
            setPageParDefaut(projet)
        }
    }

    return <div className={styles.projet}>
                <Helmet>
                    <title>Projets</title>
                    <meta name="description" content="Différents projets auxquels j'ai travaillé"/>
                </Helmet>  
                <TypesDeProjets changePage={changePage} />

                {pageParDefaut === 'projetweb' &&
                    <ProjetsWebs />
                }

                {pageParDefaut === 'projetmobile' &&
                    <ProjetsMobiles />
                }
                {pageParDefaut === 'projetbureau' &&
                    <ProjetsBureaux />
                }
                
            </div>
}