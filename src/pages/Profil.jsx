import BaseDonnees from '../components/BaseDonnees';
import Developpement from '../components/Developpement';
import Environnements from '../components/Environnements';
import { Helmet } from 'react-helmet';


import styles from './profil.module.css'

export default function Profil() {
    return <div className={styles.profil}>
                <Helmet>
                    <title>Profil</title>
                </Helmet>
                <div className={styles.competence}>
                    <div className={styles.title}>
                        <p>Bases de données</p>
                        <BaseDonnees />
                    </div>
                    <div className={styles.title}>
                        <p>Technologies</p>
                        <Developpement />
                    </div>
                    <div className={styles.title}>
                        <p>Environnements et Éditeurs</p>
                        <Environnements />
                    </div>
                </div>
                <div className={styles.marquee}>
                        <p> Présentement, j'apprends à utiliser les frameworks Django, Asp .net Core et Spring </p>
                </div>
            </div>
}