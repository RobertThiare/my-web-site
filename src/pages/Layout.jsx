
import { Outlet } from "react-router";
import Footer from "../components/Footer";
import Header from "../components/Header";

import styles from './Layout.module.css'

export default function Layout() {
    return <div className={styles.layout}>
                <Header/>
                <div className={styles.outlet}>
                    <Outlet/>
                </div>
                <Footer />
            </div>
}