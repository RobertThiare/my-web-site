import PhotoProfil from "../components/PhotoProfil";
import MaBio from '../components/MaBio';
import { Helmet } from "react-helmet";

import styles from './Biographie.module.css'

export default function Accueil () {
    return <div className={styles.biographie}>
                    <Helmet>
                        <title>Biographie</title>
                    </Helmet>
                    <div className={styles.profil}>
                    <PhotoProfil /> 
                    <MaBio/>
                    </div>
    
            </div>
}